// https://firebase.google.com/docs/functions/write-firebase-functions

const express = require('express')
const cors = require('cors')

const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

const app = express()

// Automatically allow cross-origin requests
app.use(cors({ origin: true }))

app.post('/user', (req, res) => {

  let userId

  // validate the parameters
  if (!req.body.email || typeof req.body.email != 'string') {
    return res.send(400, {
      message: 'Email is required to create a user'
    })
  }
  if (!req.body.username || typeof req.body.username != 'string') {
    return res.send(400, {
      message: 'Username is required to create a user'
    })
  }
  if (!req.body.password || typeof req.body.password != 'string') {
    return res.send(400, {
      message: 'Password is required to create a user'
    })
  }

  if (req.body.username.length < 4) {
    return res.send(400, {
      message: 'Username must be at least 4 characters'
    })
  }
  if (req.body.password.length < 6) {
    return res.send(400, {
      message: 'Password must be at least 6 characters'
    })
  }

  if (req.body.email.length > 100) {
    return res.send(400, {
      message: 'Email cannot be longer than 100 characters'
    })
  }
  if (req.body.username.length > 14) {
    return res.send(400, {
      message: 'Username cannot be longer than 14 characters'
    })
  }
  if (req.body.password.length > 100) {
    return res.send(400, {
      message: 'Password cannot be longer than 100 characters'
    })
  }

  // get the parameters
  const email = req.body.email.trim().toLowerCase()
  const username = req.body.username.trim()
  const password = req.body.password

  const usernameLowercase = username.toLowerCase()
  admin.database().ref('users').once('value').then(snapshot => {
    const users = snapshot.val()

    // check that proposed username is unique
    if (users) {
      const doesUsernameExist = Object.keys(users)
      .map(key => users[key])
      .map(user => user.username.toLowerCase())
      .includes(username)

      if (doesUsernameExist) {
        return res.send(400, {
          message: 'A user with that username already exists'
        })
      }
    }

    // create the user
    return admin.auth().createUser({email, password}).then((result) => {
      userId = result.uid

      // add the metadata for the user to /users/{uid}
      return admin.database().ref(`users/${userId}`).set({
        id: userId,
        username,
        email
      }).then(() => {
        return res.send(201, {
          message: 'User has been successfully created',
          data: { userId }
        })
      }).catch((err) => {
        return res.send(500, {
          message: err.message
        })
      })
    })

  }).catch((err) => {
    return res.send(500, {
      message: err.message
    })
  })
})

exports.apiv1 = functions.https.onRequest(app)
