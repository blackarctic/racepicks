# Design Doc | Racepicks

**Version 0.1.0** - Last updated on **Jan 3 '18** by **Nick Galloway**

> Our mission is to enhance the experience of watching NASCAR through a fantasy picks app. 

## Goals

- Provide a platform that enhances the experience of NASCAR through a fun, competitive environment.

## Functionality

### App Sections

- Cup
    - See your group by standing (W/L record)
    - See who you are up against this race
    - Click on a user to see their picks
    - Update cup settings
- Picks
    - Do your picks
    - Update User settings
- Race
    - See live race results in picks format with you and your competition
- (Admin)
    - If user is an admin, this will also be available
    - Manage
        - Races
        - Cups
        - Users

### Cups

- You must create or join a cup to play
- Public cups
    - Free
    - Support up to 100 members
- Private cups
    - Cost per 5 members
    - Hidden with a passcode for entry
        - You are given the cup passcode from the cup creator
            - This will be found in the cup section in that user's app
        - Enter this passcode when asked to join a cup

### Join Multiple Cups (Future Feature)

The platform should allow a user to join multiple cups. However, this will need some moderation so the system is not abused.

- Possibly a max of 10 cups per user per season?


### Picks

- Picks
    - 2 A List Drivers
    - 2 B List Drivers
    - 1 C List Driver
- Wagers
    - 3 Wagers
    - Choose 1 of 2 driver choices
    - These are always optional. Penalty if you lose.
- Tossups
    - 3 Tossups
    - Choose an answer

### Guiding Users

The app should guide the user through all processes. This includes the picks process, the cup choosing process, the cup creating process, etc.

### Alerts

The app will support email alerts when a user joins or leaves your cup.

The app will support texting alerts to users before picks are locked in, when race starts, and when race ends.

The app will also support texting the cup creator that a user has not done their picks 30 min before picks lock.

### Customizable team name

Choose your own team name and change it at any time

### Customizable icon

### 1 v 1

- Once the season starts, cups are locked.
- Every week you play against one other member of your cup
- Your record is what determines your standing in the cup
- The Chase for the Cup is 10 races.
    - TODO figure out how elimination should work (mimic chase format)


## MVP

- The MVP needs to be as simple as possible to get a product out the door.
- The MVP will not feature cups, rather everyone will be in one large cup.
- The app will require a beta key to sign up during the MVP stage.

- Admin
    - Manage races
    - Manage users

- Signup / Login (with Auth0)
- App
    - Cup
        - See the group standings
    - Picks
        - Select / View your picks

## Technology

- Hosting
    - Firebase
- Backend
    - Serverless with Firebase / Google Cloud Platform
- Authentication
    - Auth0
- Frontend
    - Vue with Vuex & Vue Router
- Database
    - Firebase
