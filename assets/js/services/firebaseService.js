import axios from 'axios'
import { extractResponse } from './UtilService'

class FirebaseService {
  constructor (store) {
    firebase.auth().onAuthStateChanged((user) => {

      // if not logged in, redirect to login page
      if (!user && (
        window.location.pathname !== '/landing' &&
        window.location.pathname !== '/landing/')
      ) {
        window.location = '/landing'
      }

      // if logged in, redirect to app
      else if (user && (
        window.location.pathname === '/landing' ||
        window.location.pathname === '/landing/')
      ) {
        window.location = '/'

      } else {
        this.store = store

        // if user, get the rest of the user's data
        if (user) {
          this.getUser(user.uid).then((userData) => {
            user.data = userData
            store.dispatch('user/logged_in', user)
          })
        } else {
          store.dispatch('user/logged_out')
        }
      }
    })
  }

  createUser (username, email, password) {
    return new Promise((resolve, reject) => {
      axios.post('https://us-central1-racepicks-2.cloudfunctions.net/apiv1/user', {
        username,
        email,
        password
      }).then((res) => {
        resolve(extractResponse(res))
      }).catch((err) => {
        if (!err.response) { reject(err) }
        reject(extractResponse(err.response))
      })
    })
  }

  getUser (userId) {
    return new Promise((resolve, reject) => {
      return firebase.database().ref(`users/${userId}`).once('value').then(snapshot => {
        resolve(snapshot.val() || {})
      }).catch(reject)
    })
  }

  resetPassword (email) {
    return firebase.auth()
      .sendPasswordResetEmail(email)
  }

  login (email, password) {
    return firebase.auth()
      .signInWithEmailAndPassword(email, password)
  }

  logout () {
    return firebase.auth().signOut().then(() => {
      window.location = '/landing'
    })
  }
}

export default FirebaseService
