
export const extractResponse = (res) => {
  const resBuffer = {
    status: 500,
    message: 'An error has occured',
    data: {}
  }
  if (res) {
    if (res.status) {
      resBuffer.code = res.status
    }
    if (res.message) {
      resBuffer.message = res.message
    }
    if (res.data) {
      if (res.data.status) {
        resBuffer.status = res.data.status
      }
      if (res.data.message) {
        resBuffer.message = res.data.message
      }
      if (res.data.data) {
        resBuffer.data = res.data
      }
    }
  }
  return resBuffer
}
