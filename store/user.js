import _ from 'lodash'

export const state = () => ({
  isAuthFinished: false,
  user: null
})

export const actions = {
  logged_in: (context, user) => {
    if (!user) { throw new Error('user/user_logged_in action: requires a user parameter') }
    context.commit('set_user', user)
    context.commit('mark_auth_finished')
  },
  logged_out: (context) => {
    context.commit('set_user', null)
    context.commit('mark_auth_finished')
  }
}

export const mutations = {
  set_user (state, user) {
    state.user = _.cloneDeep(user)
  },
  mark_auth_finished (state) {
    state.isAuthFinished = true
  }
}
