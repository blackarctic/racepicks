import _ from 'lodash'

export const state = () => ({
  page: ''
})

export const actions = {
  page_loaded: (context, page) => {
    context.commit('set_page', page)
  },
}

export const mutations = {
  set_page: (state, page) => {
    state.page = page
  }
}
