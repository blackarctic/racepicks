module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'RacePicks',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'An app for race fans' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Russo+One' }
    ],
    script: [
      { src: 'https://www.gstatic.com/firebasejs/4.9.0/firebase.js' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  router: {
    extendRoutes (routes, resolve) {
      routes.push({
        name: 'Home',
        path: '/',
        component: resolve(__dirname, 'pages/cup.vue')
      })
    }
  },
  mode: 'spa',
  modules: [
    '@/modules/normalize-css',
    'bootstrap-vue/nuxt'
  ]
}
